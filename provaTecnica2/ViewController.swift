//
//  ViewController.swift
//  provaTecnica2
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

struct Musica{
    let titulo: String
    let album: String
    let cantor: String
    let  imagemPequena: String
    let imagemGrande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaMusica:[Musica] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! Celula
        let musica = self.listaMusica[indexPath.row]
        
        cell.titulo.text = musica.titulo
        cell.album.text = musica.album
        cell.cantor.text = musica.cantor
        cell.imagem.image = UIImage(named: musica.imagemPequena)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaMusica[indice]
        
        detalhesViewController.nomeImagem = musica.imagemGrande
        detalhesViewController.nomeMusica = musica.titulo
        detalhesViewController.nomeAlbum = musica.album
        detalhesViewController.nomeCantor = musica.cantor
    }

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        self.listaMusica.append(Musica(titulo: "Tiro ao Álvaro",album: "Álbum Adoniran Barbosa e Convidados", cantor: "Adoniran Barbosa", imagemPequena: "capa_adoniran_pequeno",imagemGrande: "capa_adoniran_pequeno"))
        self.listaMusica.append(Musica(titulo: "Pontos Cardeais",album: "Álbum Vivo!", cantor: "Alceu Valença", imagemPequena: "capa_alceu_pequeno",imagemGrande: "capa_alceu_grande"))
        self.listaMusica.append(Musica(titulo: "Monor Abandonado",album: "Álbum Patota de Cosme", cantor: "Zeca Pagodinho", imagemPequena: "capa_zeca_pequeno",imagemGrande: "capa_zeca_grande"))
    }


}

